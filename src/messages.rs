use tictactoe_board::{Grid, GridLine, GridLineDirection, Position};
// NOTE I would rather use void::Void, but it doesn't have these derives, and
// it's quite a hassle to fix those issues (serde has workarounds, but when
// I want to serialize/deserialize `Result`s containing `Void`, those
// workarounds get quite ugly/boilerplatey.
#[derive(Debug, Serialize, Deserialize)]
pub enum Never {}

pub struct Lobby {
	pub title: String,
	pub owner: UserId,
	pub guest: Option<UserId>,
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(remote = "GridLineDirection")]
enum GridLineDirectionDef {
	Horizontal,
	Vertical,
	MainDiagonal,
	AntiDiagonal,
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(remote = "GridLine")]
struct GridLineDef {
	#[serde(with = "PositionDef")]
	start: Position,
	#[serde(with = "GridLineDirectionDef")]
	direction: GridLineDirection,
	length: u8,
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(remote = "Position")]
struct PositionDef(i8, i8);

impl Lobby {
	pub fn new(owner: UserId, title: String) -> Self {
		Lobby {
			title,
			owner,
			guest: None,
		}
	}

	pub fn set_title(&mut self, title: String) {
		self.title = title;
	}

	pub fn remove_guest(&mut self) -> Option<UserId> {
		self.guest.take()
	}

	pub fn add_guest(&mut self, guest: UserId) -> bool {
		if guest == self.owner || self.guest.is_some() {
			false
		} else {
			self.guest = Some(guest);
			true
		}
	}

	pub fn into_game(self) -> Game {
		Game::new(self.title, [self.owner, self.guest.unwrap()])
	}
}

// TODO Add the board state! (Probably start using the old Grid code)
pub struct Game {
	pub title: String,
	pub players: [UserId; 2],
	pub active_player: u8,
	pub grid: Grid,
}

impl Game {
	pub fn new(title: String, players: [UserId; 2]) -> Self {
		Game {
			title,
			players,
			active_player: 0,
			grid: Grid::new(3, 3),
		}
	}

	/// Returns whose turn it is next, if the move was valid.
	pub fn try_move(
		&mut self,
		who: UserId,
		position: Position,
	) -> Result<(UserId, Option<(UserId, GridLine)>), MakeMoveError> {
		match self.players.iter().position(|&uid| uid == who) {
			Some(player_index) => {
				if player_index as u8 == self.active_player {
					if self.grid.position_is_in_bounds(position) {
						if self.grid[position].is_none() {
							self.grid[position] = Some(self.active_player.into());
							let winner = if let Some(winning_path) =
								self.grid.check_if_winning_move(position)
							{
								Some((self.players[self.active_player as usize], winning_path))
							} else {
								None
							};
							self.active_player = (self.active_player + 1) % 2;
							Ok((self.players[self.active_player as usize], winner))
						} else {
							Err(MakeMoveError::InvalidMove)
						}
					} else {
						Err(MakeMoveError::InvalidMove)
					}
				} else {
					Err(MakeMoveError::NotYourTurn)
				}
			}
			None => Err(MakeMoveError::InvalidGame),
		}
	}

	pub fn get_players(&self) -> &[UserId; 2] {
		&self.players
	}
}

#[derive(Serialize, Deserialize, PartialEq, Debug, Clone, Copy, Eq, Hash)]
pub struct UserId(pub u32);

impl From<UserId> for usize {
	fn from(src: UserId) -> Self {
		src.0 as usize
	}
}

impl From<usize> for UserId {
	fn from(src: usize) -> Self {
		UserId(src as u32)
	}
}

#[derive(Serialize, Deserialize, PartialEq, Debug, Clone, Copy, Eq, Hash)]
pub struct LobbyId(pub u32);

impl From<LobbyId> for usize {
	fn from(src: LobbyId) -> Self {
		src.0 as usize
	}
}

impl From<usize> for LobbyId {
	fn from(src: usize) -> Self {
		LobbyId(src as u32)
	}
}

#[derive(Serialize, Deserialize, PartialEq, Debug, Clone, Copy, Eq, Hash)]
pub struct GameId(pub u32);

impl From<GameId> for usize {
	fn from(src: GameId) -> Self {
		src.0 as usize
	}
}

impl From<usize> for GameId {
	fn from(src: usize) -> Self {
		GameId(src as u32)
	}
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone, Copy)]
pub enum ChangeTitleError {
	NoAuthority,
	InvalidLobby,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone, Copy)]
pub enum DestroyLobbyError {
	NoAuthority,
	InvalidLobby,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone, Copy)]
pub enum JoinLobbyError {
	/// Either the lobby already had a guest, or the owner tried to join
	/// his own lobby.
	InvalidJoin,
	InvalidLobby,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone, Copy)]
pub enum KickGuestError {
	NoAuthority,
	NoGuest,
	InvalidLobby,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone, Copy)]
pub enum StartGameError {
	NoAuthority,
	NoGuest,
	InvalidLobby,
}

#[derive(Serialize, Deserialize, Debug)]
pub enum AnonRequest {
	Login { user_name: String },
}

#[derive(Serialize, Deserialize, Debug)]
pub enum AnonResponse {
	Login(UserId),
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub enum UserRequest {
	ChangeTitle(LobbyId, String),
	Concede(GameId),
	CreateLobby(String),
	DestroyLobby(LobbyId),
	JoinLobby(LobbyId),
	KickGuest(LobbyId),
	MakeMove(GameId, #[serde(with = "PositionDef")] Position),
	StartGame(LobbyId),
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub enum MakeMoveError {
	/// The game does not exist, or the player is not in specified game
	InvalidGame,
	/// Move is out of bounds or in a non-empty cell
	InvalidMove,
	NotYourTurn,
}

#[derive(Serialize, Deserialize, Debug)]
pub enum ConcedeError {
	/// The game does not exist, or the player is not in specified game
	InvalidGame,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub enum GameResult {
	PlayerConceded(u8),
	PlayerDisconnected(u8),
	PlayerWon(
		UserId,
		#[serde(with = "GridLineDef")] GridLine,
	),
}

// TODO We currently have a duality here that I'm not sure is necessary. We
// have several cases where we send a stripped-down version of a message to
// clients that already have some of the information we're sending, simply to
// avoid sending any redundant information. This barely saves us anything, but
// it adds complexity to both the server logic and client logic.
// If we were to replace the system, we'd still need to tell the client sending
// the request whether his request failed or not, which is pretty much what
// we're doing now; but to make client and server logic simpler, we could just
// send an empty `Ok(())`, and then let the client handle the result just like
// all the other clients. (e.g. if he requested CreateLobby, he could receive
// an Ok(()) response, followed by LobbyCreated { ... }. I think that would be
// preferable, to remove some duplication. Sending a few more bytes over the
// network is not a problem.
#[derive(Serialize, Deserialize, Debug)]
pub enum UserResponse {
	ChangeTitle(Result<(), ChangeTitleError>),
	CreateLobby(Result<LobbyId, Never>),
	DestroyLobby(Result<(), DestroyLobbyError>),
	JoinLobby(Result<(), JoinLobbyError>),
	KickGuest(Result<(), KickGuestError>),
	StartGame(Result<GameId, StartGameError>),
	MakeMove(Result<(), MakeMoveError>),
	Concede(Result<(), ConcedeError>),

	GameFinished {
		game: GameId,
		reason: GameResult,
	},
	GameStarted {
		lobby: LobbyId,
		id: GameId,
	},
	LobbyCreated {
		id: LobbyId,
		title: String,
		owner: UserId,
	},
	LobbyDestroyed {
		lobby: LobbyId,
	},
	LobbyJoined {
		lobby: LobbyId,
		user: UserId,
	},
	LobbyParted {
		lobby: LobbyId,
	},
	LobbyTitleChanged {
		lobby: LobbyId,
		new_title: String,
	},
	OpponentMoved {
		game: GameId,
		#[serde(with = "PositionDef")]
		position: Position,
	},
	UserConnected {
		id: UserId,
		name: String,
	},
	UserDisconnected {
		user: UserId,
	},
}
