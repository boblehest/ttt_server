#[macro_use]
extern crate log;
#[macro_use]
extern crate futures;

use bincode::{deserialize, serialize_into};
use bytes::BytesMut;
use futures::Stream;
use slab::Slab;
use tokio::{
	runtime::current_thread,
	io::{self, ReadHalf, WriteHalf},
	net::{TcpListener, TcpStream},
	prelude::*,
};

use std::cell::RefCell;
use std::rc::Rc;

use server::codec::length_delimited::{FramedReader, FramedWriter};
use server::messages::*;

struct World {
	pub users: Slab<User>,
	pub lobbies: Slab<Lobby>,
	pub games: Slab<Game>,
}

impl World {
	fn new() -> Self {
		World {
			users: Slab::new(),
			lobbies: Slab::new(),
			games: Slab::new(),
		}
	}

	fn add_user(&mut self, user: User) -> UserId {
		UserId(self.users.insert(user) as u32)
	}

	fn get_user(&mut self, user: UserId) -> &mut User {
		&mut self.users[user.into()]
	}

	fn remove_user(&mut self, user: UserId) {
		self.users.remove(user.into());
	}

	fn start_game(&mut self, lobby: LobbyId) -> GameId {
		let lobby = self.lobbies.remove(lobby.into());
		GameId(self.games.insert(lobby.into_game()) as u32)
	}

	fn destroy_game(&mut self, game: GameId) -> Game {
		self.games.remove(game.into())
	}

	fn create_lobby(&mut self, owner: UserId, title: String) -> LobbyId {
		LobbyId(self.lobbies.insert(Lobby::new(owner, title)) as u32)
	}

	fn destroy_lobby(&mut self, lobby: LobbyId) {
		self.lobbies.remove(lobby.into());
	}

	fn get_lobby(&mut self, id: LobbyId) -> Option<&mut Lobby> {
		self.lobbies.get_mut(id.into())
	}

	fn iter_users(&mut self) -> impl Iterator<Item = (UserId, &mut User)> {
		self.users.iter_mut().map(|(id, u)| (id.into(), u))
	}
}

struct User {
	name: String,
	tx: WriteHalf<TcpStream>,
}

impl User {
	fn new(name: String, tx: WriteHalf<TcpStream>) -> Self {
		User { name, tx }
	}
}

enum ClientState {
	NotLoggedIn { tx: WriteHalf<TcpStream> },
	LoggedIn { user_id: UserId },
}

struct Client {
	state: ClientState,
	world: Rc<RefCell<World>>,
}

impl Drop for Client {
	fn drop(&mut self) {
		if let ClientState::LoggedIn { user_id } = self.state {
			let mut world = self.world.borrow_mut();
			world.remove_user(user_id);

			let mut writer = FramedWriter::new();

			// TODO In a better world, this cleanup doesn't happen in here.
			// The "world" should ensure its own consistency without relying
			// on external code to maintain its invariants.

			// Clean up lobbies owned by this client
			let lobbies = world
				.lobbies
				.iter()
				.filter(|(_, lobby)| lobby.owner == user_id)
				.map(|(id, _)| id.into())
				.collect::<Vec<_>>();

			for lobby in lobbies {
				world.destroy_lobby(lobby);
				let msg = UserResponse::LobbyDestroyed { lobby };
				let packet = writer.write_with(|w| serialize_into(w, &msg)).unwrap();
				for (_, user) in world.users.iter_mut() {
					match user.tx.write_all(&packet) {
						Ok(()) => (),
						Err(e) => {
							info!("Failed to send LobbyDestroyed message to client: {}", e);
						}
					}
				}
			}

			// Clean up games owned by this client
			let game_ids = world
				.games
				.iter()
				.filter(|(_, game)| game.players.contains(&user_id))
				.map(|(id, _)| id.into())
				.collect::<Vec<_>>();

			for game_id in game_ids {
				let game = world.destroy_game(game_id);

				let (loser_index, other_player_id) = if user_id == game.players[0] {
					(0, game.players[1])
				} else {
					(1, game.players[0])
				};

				let msg = UserResponse::GameFinished {
					game: game_id,
					reason: GameResult::PlayerDisconnected(loser_index),
				};
				let packet = writer.write_with(|w| serialize_into(w, &msg)).unwrap();
				match world.users[other_player_id.into()].tx.write_all(&packet) {
					Ok(()) => (),
					Err(e) => {
						info!("Failed to send GameFinished message to client: {}", e);
					}
				}
			}

			// Tell others that this client left
			let msg = UserResponse::UserDisconnected { user: user_id };
			let packet = writer.write_with(|w| serialize_into(w, &msg)).unwrap();

			for (_, user) in world.users.iter_mut() {
				match user.tx.write_all(&packet) {
					Ok(()) => (),
					Err(e) => {
						info!("Failed to send UserDisconnected message to client: {}", e);
					}
				}
			}
		}
	}
}

impl Client {
	fn new(tx: WriteHalf<TcpStream>, world: Rc<RefCell<World>>) -> Self {
		Client {
			state: ClientState::NotLoggedIn { tx },
			world,
		}
	}

	// TODO When we start getting more states, we can extract them out into their
	// own structs, with impls for
	// trait State { type Msg: Deserialize; fn handle(&mut self, msg: Self::Msg) }
	// But DO NOT split this """monolith""" until we actually notice any
	// disadvantages. Splitting it increases complexity a lot, especially when
	// we do it before we have solved all other problems (like "how do we send
	// replies out from a state in a clean way?")
	fn handle_message(&mut self, message: &[u8]) -> Result<(), bincode::Error> {
		let mut world = self.world.borrow_mut();
		// Used when transitioning states, since we can't do the transition
		// inside the match arm (since we're borrowing the state there.)
		let mut login = None;

		match self.state {
			ClientState::NotLoggedIn { .. } => {
				let message = deserialize::<AnonRequest>(message)?;
				debug!("Got message: {:?}", message);

				let AnonRequest::Login { user_name } = message;
				login = Some(user_name);
			}
			ClientState::LoggedIn { user_id } => {
				let message = deserialize::<UserRequest>(message)?;
				debug!(
					"Got message from user \"{}\" ({}): {:?}",
					world.get_user(user_id).name,
					user_id.0,
					message
				);

				match message {
					UserRequest::ChangeTitle(lobby_id, title) => {
						let result = {
							match world.get_lobby(lobby_id) {
								Some(lobby) => if lobby.owner != user_id {
									Err(ChangeTitleError::NoAuthority)
								} else {
									lobby.set_title(title.clone());
									Ok(())
								},
								None => Err(ChangeTitleError::InvalidLobby),
							}
						};

						let mut writer = FramedWriter::new();

						let response_msg = UserResponse::ChangeTitle(result);
						let response_packet = writer
							.write_with(|w| serialize_into(w, &response_msg))
							.unwrap();

						if result.is_err() {
							let tx = &mut world.get_user(user_id).tx;
							tx.write_all(&response_packet).unwrap();
						} else {
							let broadcast_msg = UserResponse::LobbyTitleChanged {
								lobby: lobby_id,
								new_title: title,
							};
							let broadcast_packet = writer
								.write_with(|w| serialize_into(w, &broadcast_msg))
								.unwrap();

							for (id, user) in world.iter_users() {
								let packet = if id == user_id {
									&response_packet
								} else {
									&broadcast_packet
								};

								user.tx.write_all(packet).unwrap();
							}
						}
					}
					UserRequest::Concede(game) => {
						let result = match world.games.get_mut(game.into()) {
							Some(game) => {
								let pindex = game.players.iter().position(|&uid| uid == user_id);
								if let Some(pindex) = pindex {
									let opponent_pindex = (pindex + 1) % 2;
									let opponent_uid = game.players[opponent_pindex];
									Ok((pindex as u8, opponent_uid))
								} else {
									Err(ConcedeError::InvalidGame)
								}
							}
							_ => Err(ConcedeError::InvalidGame),
						};

						let mut writer = FramedWriter::new();

						let result = result.map(|(pindex, opponent_user_id)| {
								let msg = UserResponse::GameFinished {
									game,
									reason: GameResult::PlayerConceded(pindex),
								};
								let packet = writer
									.write_with(|w| serialize_into(w, &msg))
									.unwrap();
								let tx = &mut world.get_user(opponent_user_id).tx;
								tx.write_all(&packet).unwrap();
						});

						{
							let response_msg = UserResponse::Concede(result);
							let response_packet = writer
								.write_with(|w| serialize_into(w, &response_msg))
								.unwrap();
							let tx = &mut world.get_user(user_id).tx;
							tx.write_all(&response_packet).unwrap();
						}
					}
					UserRequest::CreateLobby(title) => {
						let lobby_id = world.create_lobby(user_id, title.clone());
						let mut writer = FramedWriter::new();

						let response_msg = UserResponse::CreateLobby(Ok(lobby_id));
						let broadcast_msg = UserResponse::LobbyCreated {
							id: lobby_id,
							title,
							owner: user_id,
						};

						let response_packet = writer
							.write_with(|w| serialize_into(w, &response_msg))
							.unwrap();
						let broadcast_packet = writer
							.write_with(|w| serialize_into(w, &broadcast_msg))
							.unwrap();

						for (id, user) in world.iter_users() {
							let packet = if id == user_id {
								&response_packet
							} else {
								&broadcast_packet
							};

							user.tx.write_all(packet).unwrap();
						}
					}
					UserRequest::DestroyLobby(lobby_id) => {
						let result = {
							match world.get_lobby(lobby_id) {
								Some(lobby) => if lobby.owner != user_id {
									Err(DestroyLobbyError::NoAuthority)
								} else {
									Ok(())
								},
								None => Err(DestroyLobbyError::InvalidLobby),
							}
						};

						// Actually perform the action
						if result.is_ok() {
							world.destroy_lobby(lobby_id);
						}

						let mut writer = FramedWriter::new();

						let response_msg = UserResponse::DestroyLobby(result);
						let response_packet = writer
							.write_with(|w| serialize_into(w, &response_msg))
							.unwrap();

						if result.is_err() {
							let tx = &mut world.get_user(user_id).tx;
							tx.write_all(&response_packet).unwrap();
						} else {
							let broadcast_msg = UserResponse::LobbyDestroyed { lobby: lobby_id };
							let broadcast_packet = writer
								.write_with(|w| serialize_into(w, &broadcast_msg))
								.unwrap();

							for (id, user) in world.iter_users() {
								let packet = if id == user_id {
									&response_packet
								} else {
									&broadcast_packet
								};

								user.tx.write_all(packet).unwrap();
							}
						}
					}
					UserRequest::JoinLobby(lobby_id) => {
						let result = {
							match world.get_lobby(lobby_id) {
								Some(lobby) => if lobby.add_guest(user_id) {
									Ok(())
								} else {
									Err(JoinLobbyError::InvalidJoin)
								},
								None => Err(JoinLobbyError::InvalidLobby),
							}
						};

						let mut writer = FramedWriter::new();

						let response_packet = {
							let msg = UserResponse::JoinLobby(result);

							writer.write_with(|w| serialize_into(w, &msg)).unwrap()
						};

						if result.is_err() {
							let tx = &mut world.get_user(user_id).tx;
							tx.write_all(&response_packet).unwrap();
						} else {
							let broadcast_packet = {
								let msg = UserResponse::LobbyJoined {
									lobby: lobby_id,
									user: user_id,
								};

								writer.write_with(|w| serialize_into(w, &msg)).unwrap()
							};

							for (id, user) in world.iter_users() {
								let packet = if id == user_id {
									&response_packet
								} else {
									&broadcast_packet
								};

								user.tx.write_all(packet).unwrap();
							}
						}
					}
					UserRequest::KickGuest(lobby_id) => {
						let result = {
							match world.get_lobby(lobby_id) {
								Some(lobby) => {
									if lobby.owner != user_id {
										Err(KickGuestError::NoAuthority)
									} else if lobby.remove_guest().is_some() {
										Ok(())
									} else {
										Err(KickGuestError::NoGuest)
									}
								}
								None => Err(KickGuestError::InvalidLobby),
							}
						};

						let mut writer = FramedWriter::new();

						let response_msg = UserResponse::KickGuest(result);
						let response_packet = writer
							.write_with(|w| serialize_into(w, &response_msg))
							.unwrap();

						if result.is_err() {
							let tx = &mut world.get_user(user_id).tx;
							tx.write_all(&response_packet).unwrap();
						} else {
							let broadcast_packet = {
								let msg = UserResponse::LobbyParted { lobby: lobby_id };

								writer.write_with(|w| serialize_into(w, &msg)).unwrap()
							};

							for (id, user) in world.iter_users() {
								let packet = if id == user_id {
									&response_packet
								} else {
									&broadcast_packet
								};

								user.tx.write_all(packet).unwrap();
							}
						}
					}
					UserRequest::MakeMove(game, position) => {
						let result = match world.games.get_mut(game.into()) {
							Some(game) => game.try_move(user_id, position),
							None => Err(MakeMoveError::InvalidGame),
						};

						let mut writer = FramedWriter::new();

						let response_msg = UserResponse::MakeMove(result.clone().map(|_| ()));
						let response_packet = writer
							.write_with(|w| serialize_into(w, &response_msg))
							.unwrap();
						let tx = &mut world.get_user(user_id).tx;
						tx.write_all(&response_packet).unwrap();

						if let Ok((opponent_user_id, winner)) = result {
							{
								let msg = UserResponse::OpponentMoved {
									game,
									position,
								};
								let packet = writer
									.write_with(|w| serialize_into(w, &msg))
									.unwrap();
								let tx = &mut world.get_user(opponent_user_id).tx;
								tx.write_all(&packet).unwrap();
							}

							if let Some((winner, winning_path)) = winner {
								let msg = UserResponse::GameFinished {
									game,
									reason: GameResult::PlayerWon(winner,
																  winning_path),
								};
								let packet = writer
									.write_with(|w| serialize_into(w, &msg))
									.unwrap();

								for user in &[user_id, opponent_user_id] {
									let tx = &mut world.get_user(*user).tx;
									tx.write_all(&packet).unwrap();
								}

								world.destroy_game(game);
							}
						}
					}
					UserRequest::StartGame(lobby_id) => {
						let result = {
							match world.get_lobby(lobby_id) {
								Some(lobby) => {
									if lobby.owner != user_id {
										Err(StartGameError::NoAuthority)
									} else if lobby.guest.is_some() {
										Ok(())
									} else {
										Err(StartGameError::NoGuest)
									}
								},
								None => Err(StartGameError::InvalidLobby),
							}
						};

						let result = result.map(|()| {
							world.start_game(lobby_id)
						});

						let mut writer = FramedWriter::new();

						{
							let response_msg = UserResponse::StartGame(result);
							let response_packet = writer
								.write_with(|w| serialize_into(w, &response_msg))
								.unwrap();

							let tx = &mut world.get_user(user_id).tx;
							tx.write_all(&response_packet).unwrap();
						}

						if let Ok(game) = result {
							let broadcast_packet = {
								let msg = UserResponse::GameStarted {
									lobby: lobby_id,
									id: game,
								};

								writer.write_with(|w| serialize_into(w, &msg)).unwrap()
							};

							for (id, user) in world.iter_users() {
								if id == user_id {
									continue;
								};

								user.tx.write_all(&broadcast_packet).unwrap();
							}
						}
					}
				}
			}
		}

		if let Some(user_name) = login {
			take_mut::take(&mut self.state, |old| match old {
				ClientState::NotLoggedIn { tx } => {
					let user = User::new(user_name.clone(), tx);
					let user_id = world.add_user(user);
					ClientState::LoggedIn { user_id }
				}
				_ => unreachable!(),
			});

			let user_id = match self.state {
				ClientState::LoggedIn { user_id } => user_id,
				_ => unreachable!(),
			};

			let World { users, lobbies, .. } = &mut *world;

			let mut writer = FramedWriter::new();
			{
				let mut other_users = users
					.iter()
					.map(|(id, u)| (id.into(), u.name.clone()))
					.filter(|(id, _)| *id != user_id)
					.collect::<Vec<_>>();

				let tx = &mut users[user_id.into()].tx;

				// Send response
				{
					let login_response = AnonResponse::Login(user_id);
					let response_packet = writer
						.write_with(|w| serialize_into(w, &login_response))
						.unwrap();
					tx.write_all(&response_packet).unwrap();
				}

				// NOTE The previous message moved the user into the "LoggedIn"
				// state, so we can now send messages to that state. Could
				// we formalize this in the code? (I.e. make the type system
				// enforce that we only send correctly typed messages)

				// Send list of lobbies
				for (lobby_id, lobby) in lobbies.iter() {
					let msg = UserResponse::LobbyCreated {
						id: lobby_id.into(),
						title: lobby.title.clone(),
						owner: lobby.owner,
					};
					let packet = writer.write_with(|w| serialize_into(w, &msg)).unwrap();
					tx.write_all(&packet).unwrap();

					if let Some(guest) = lobby.guest {
						let msg = UserResponse::LobbyJoined {
							lobby: lobby_id.into(),
							user: guest,
						};

						let packet = writer.write_with(|w| serialize_into(w, &msg)).unwrap();
						tx.write_all(&packet).unwrap();
					}
				}

				// Send list of users
				for (id, name) in other_users.drain(..) {
					let msg = UserResponse::UserConnected { id, name };
					let packet = writer.write_with(|w| serialize_into(w, &msg)).unwrap();
					tx.write_all(&packet).unwrap();
				}
			}

			// Broadcast event
			let msg = UserResponse::UserConnected {
				id: user_id,
				name: user_name,
			};
			let packet = writer.write_with(|w| serialize_into(w, &msg)).unwrap();

			for (_, user) in users.iter_mut() {
				user.tx.write_all(&packet).unwrap();
			}
		}

		Ok(())
	}
}

struct LengthDelimited {
	socket: ReadHalf<TcpStream>,
	reader: FramedReader,
	rd: BytesMut,
}

impl Stream for LengthDelimited {
	type Item = BytesMut;
	type Error = io::Error;

	fn poll(&mut self) -> Result<Async<Option<Self::Item>>, Self::Error> {
		let sock_closed = self.fill_read_buf()?.is_ready();

		if let Some(msg) = self.reader.read(&mut self.rd) {
			Ok(Async::Ready(Some(msg)))
		} else if sock_closed {
			Ok(Async::Ready(None))
		} else {
			Ok(Async::NotReady)
		}
	}
}

impl LengthDelimited {
	fn new(socket: ReadHalf<TcpStream>) -> Self {
		LengthDelimited {
			socket,
			reader: FramedReader::new(),
			rd: BytesMut::new(),
		}
	}

	fn fill_read_buf(&mut self) -> Result<Async<()>, io::Error> {
		// NOTE BytesMut doesn't (as of bytes 0.4.8) automatically grow
		// when writing into it, so as of now the value we reserve here
		// will be the maximum size of messages we can handle.
		self.rd.reserve(1024 - self.rd.capacity());

		loop {
			let n = try_ready!(self.socket.read_buf(&mut self.rd));

			if n == 0 {
				return Ok(Async::Ready(()));
			}
		}
	}
}

fn main() {
	simple_logger::init().unwrap();

	let addr = "[::]:1337"
		.parse()
		.expect("Failed to parse address to listen on");
	let listener = TcpListener::bind(&addr).expect("Failed to bind listener");

	let world = Rc::new(RefCell::new(World::new()));

	let server = listener.incoming().for_each(move |socket| {
		debug!("Got client");

		let (client_rx, client_tx) = socket.split();

		let codec = LengthDelimited::new(client_rx);
		// TODO Make a future that wraps LengthDelimited and Client.
		// (If necessary. It might not be, but we'll see)
		// TODO Why are we not wrapping client_tx?
		let mut client = Client::new(client_tx, world.clone());

		let future = codec
			.for_each(move |message| {
				if client.handle_message(&message).is_err() {
					error!("Failed to decode message");
				}

				Ok(())
			})
			.map_err(|err| {
				error!("Got error: {:?}", err);
			});

		current_thread::spawn(future);

		Ok(())
	});

	current_thread::block_on_all(server).unwrap();
}
