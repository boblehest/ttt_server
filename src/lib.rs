#[macro_use]
extern crate serde_derive;
#[cfg(test)]
#[macro_use]
extern crate proptest;

pub mod messages;

pub mod codec {
	pub mod length_delimited {
		extern crate byteorder;
		extern crate bytes;

		use self::byteorder::{ByteOrder, LittleEndian};
		use self::bytes::{BufMut, BytesMut};

		use std::io;
		use std::mem::size_of;

		type LengthT = u32;

		#[derive(Default)]
		pub struct FramedReader {
			length: Option<LengthT>,
		}

		impl FramedReader {
			pub fn new() -> Self {
				Default::default()
			}

			pub fn read(&mut self, data: &mut BytesMut) -> Option<BytesMut> {
				let length = match self.length {
					Some(l) => l,
					None => {
						if data.len() < size_of::<LengthT>() {
							return None;
						}

						let length = LittleEndian::read_u32(&data.split_to(size_of::<LengthT>()));

						self.length = Some(length);

						length
					}
				};

				if data.len() < length as usize {
					return None;
				}

				self.length = None;
				Some(data.split_to(length as usize))
			}
		}

		// TODO Replace with a standalone function (or trait on BytesMut)?
		#[derive(Default)]
		pub struct FramedWriter {
			buf: BytesMut,
		}

		impl FramedWriter {
			pub fn new() -> Self {
				Default::default()
			}

			pub fn write_with<E, F>(&mut self, cb: F) -> Result<BytesMut, E>
			where
				F: Fn(&mut io::Write) -> Result<(), E>,
			{
				self.buf.reserve(1024);

				let payload = self.buf.split_off(size_of::<LengthT>());
				let mut writer = payload.writer();

				cb(&mut writer)?;

				let payload = writer.into_inner();
				self.buf.put_u32_le(payload.len() as LengthT);
				self.buf.unsplit(payload);
				Ok(self.buf.take())
			}
		}

		#[cfg(test)]
		mod test {
			use super::BytesMut;
			use super::{FramedReader, FramedWriter};
			use proptest::prelude::prop::collection::vec;
			use proptest::prelude::*;

			proptest! {
				#[test]
				fn pack_unpack(data in prop::collection::vec(0u8..=255, 0..1000)) {
					let mut writer = FramedWriter::new();

					let mut result = writer.write_with(|w| w.write_all(&data)).unwrap();

					let mut reader = FramedReader::new();
					let original = reader.read(&mut result);

					prop_assert_eq!(data, original.unwrap());
				}

				#[test]
				fn pack_unpack_stream(datas in vec(vec(0u8..=255, 0..1000), 0..4)) {
					let mut writer = FramedWriter::new();
					let mut stream = Vec::new();
					for data in &datas {
						let msg = writer.write_with(|w| w.write_all(data)).unwrap();
						stream.extend(msg);
					}

					let mut stream: BytesMut = stream.into();

					let mut reader = FramedReader::new();

					for data in datas {
						prop_assert_eq!(data, reader.read(&mut stream).unwrap());
					}
				}
			}
		}
	}
}
